import { Component, Vue } from 'vue-property-decorator';
import Task from '@/types/Task';
import WithRender from './todo-list.html';
import TodoForm from '../TodoForm/TodoForm';

@WithRender
@Component({
  components: {
    'todo-form': TodoForm,
  },
})
export default class ToDoList extends Vue {
    public tasks: Task[] = [{ description: 'test task', completed: false }];

    public addTask(description: string): void {
      this.tasks.push({ description, completed: false });
    }
}
