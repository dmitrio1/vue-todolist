import { Component, Vue, Prop } from 'vue-property-decorator';
import WithRender from './todo-form.html';

@WithRender
@Component
export default class TodoForm extends Vue {
  @Prop({ type: String, default: 'Add Task' }) readonly buttonText?: string

  public task = '';

  public emitTask(): void {
    this.$emit('added', this.task);
    this.task = '';
  }
}
